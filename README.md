# Amnesia save file editor

Amnesia-Cheat is a command-line save file editor for the game 'Amnesia: The Dark Descent' written in python.

### INSTALL

First, make sure you have **python3** installed.
Then, use the following commands to download the script and install it.

    wget "https://gitlab.com/mgriseri/amnesia-cheat/raw/master/amnesia-cheat.py"
    chmod +x amnesia-cheat.py
    sudo mv amnesia-cheat.py /usr/local/bin/amnesia-cheat

> You can uninstall it by removing the file with `sudo rm /usr/local/bin/amnesia-cheat`

### USAGE

You can use several options to modify your save file:

    amnesia-cheat --health
    amnesia-cheat --sanity
    amnesia-cheat --oil
    amnesia-cheat --tinderboxes

Each option sets the maximum value. You can then reload the game and enjoy!

### NOTES:

- The script always modifies your **most recent save file**.

- **You should make a copy of your profile folder** before using the script.

### LICENSE

GNU GENERAL PUBLIC LICENSE:

https://www.gnu.org/licenses/gpl.html

### CONTRIBUTE

If you want to adapt the source code, it is under the GPL, you're free to do so. 

If this script has been helpful to you, feel free to [send a donation](https://www.paypal.me/mgriseri).
