#!/usr/bin/env python3

"""
Cheat script for the game 'Amnesia: The Dark Descent' on Linux.
Tested with version 2.0.0.3 from GOG.com on openSUSE Leap 42.3.

LICENSE:
GNU GPL v3
https://www.gnu.org/licenses/gpl.html
"""

import argparse
import glob
import os
import sys
import xml.etree.ElementTree

SCRIPT_NAME = sys.argv[0]
SCRIPT_CACHE_NAME = 'amnesia-cheat-cache.txt'
USER_HOME = os.path.expanduser("~")
SAVEFILE_EXT = '.sav'


def get_most_recent_amnesia_save_folder():
    """
    Finds the most recent '*.sav' file with
    'amnesia' in its path and returns its folder.

    The try/except block defines where the search should start.
    The first time the script is run, it searches the *entire home directory*.
    But if save files are found, it keeps their *main folder* path in a file.
    To avoid another entire home directory scan next time.
    """
    savefiles_found = False
    savefiles_list = []
    savefiles_mostrecentdir = False

    try:
        with open(SCRIPT_CACHE_NAME, 'r') as cached_main_files_dir:
            search_start = cached_main_files_dir.read()
    except FileNotFoundError:
        print("Locating your save file. Please wait...")
        search_start = USER_HOME

    for root, dirs, files in os.walk(search_start):
        for name in files:
            if name.endswith(SAVEFILE_EXT):
                savefile_path = os.path.join(root, name)
                savefile_dir = os.path.dirname(savefile_path)
                if 'amnesia' in savefile_path.lower():
                    savefiles_found = True
                    savefiles_list += [savefile_path]

    if savefiles_found:
        savefile_mostrecent = max(savefiles_list, key=os.path.getctime)
        savefiles_mostrecentdir = os.path.dirname(savefile_mostrecent)
        savefiles_main_folder = os.path.dirname(savefiles_mostrecentdir)

        with open(SCRIPT_CACHE_NAME, 'w') as cached_main_files_dir:
            cached_main_files_dir.write(savefiles_main_folder)
    else:
        savefiles_mostrecentdir = False

    return savefiles_mostrecentdir


# MAIN CHEAT TO CHANGE PLAYER DATA
def player_cheat(player_var, new_value):
    """
    Change the value of a variable in the Player object
    """
    player_var, new_value = str(player_var), str(new_value)

    for data in savefile_data:
        name = (data.get('name'))
        if name == 'mPlayer':
            # list all elements in the player data
            data_player = data.findall('var')
            # for each, element
            for data in data_player:
                data_name = data.get('name')
                data_val = data.get('val')
                # Look for a specific element and change its value
                if data_name == player_var:
                    msg = 'Setting %s to %s...' % (data_name, new_value)
                    print(msg)
                    data.set('val', new_value)
                    # write tree object as actual xml file
                    savefile_tree.write(savefile_name)
                    print('Done!')
                    break
            break


# ARGUMENT HANDLING
parser = argparse.ArgumentParser(
    description='Script to cheat in Amnesia: The Dark Descent.')

parser.add_argument(
    '-H',
    '--health',
    action='store_true',
    help="fill health.")

parser.add_argument(
    '-s',
    '--sanity',
    action='store_true',
    help="fill sanity.")

parser.add_argument(
    '-o',
    '--oil',
    action='store_true',
    help="fill oil.")

parser.add_argument(
    '-t',
    '--tinderboxes',
    action='store_true',
    help="fill tinderboxes.")

parser.add_argument(
    '-e',
    '--everything',
    action='store_true',
    help="fill everything.")

args = parser.parse_args()


# MAIN FUNCTION
if __name__ == '__main__':
    savefile_dir = get_most_recent_amnesia_save_folder()

    if savefile_dir:
        os.chdir(savefile_dir)
    else:
        print("ERROR: No save file found.")
        sys.exit()

    # store the most recent savefile name
    savefiles_list = glob.glob('*.sav')
    savefile_name = max(savefiles_list, key=os.path.getctime)

    # create tree object from savefile
    savefile_tree = xml.etree.ElementTree.parse(savefile_name)
    savefile_data = (savefile_tree.find('class'))

    print('Applying cheat on the most recent file:')
    print(savefile_name, '\n')

    if args.health:
        player_cheat('mfHealth', 99)
    elif args.sanity:
        player_cheat('mfSanity', 99)
    elif args.oil:
        player_cheat('mfLampOil', 99)
    elif args.tinderboxes:
        player_cheat('mlTinderboxes', 99)
    elif args.everything:
        player_cheat('mfHealth', 99)
        player_cheat('mfSanity', 99)
        player_cheat('mfLampOil', 99)
        player_cheat('mlTinderboxes', 99)
    else:
        print('Something went wrong...')
        print('Make sure to read the manual or run:')
        print('$ python3 %s --help' % (SCRIPT_NAME))
